#include <stdint.h>

#define EMPTY 0
#define TAKEN !EMPTY
#define BLOCK_SIZE 4096
#define MAX_BLOCKS 1000

typedef struct {
  uint64_t memoryAvailable;
  uint8_t* bitmap;
  uint64_t* group_sizes; // Aca se guarda la cantidad de paginas reservadas por el proceso que empieza en esta posicion en el bitmap
  void * memoryBaseAddress;
} memoryManager_t;

void init_memoryManager(void * base_address, uint64_t bytes);
void* malloc(uint64_t bytes_to_reserve);
void* malloc_handler(uint64_t bytes_to_reserve, void** address);
void free_handler(void* p);
void print_bitmap();
