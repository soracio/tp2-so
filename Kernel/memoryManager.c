#include <stdint.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>
#include <SysCallDispatcher.h>
#include <memoryManager.h>

static memoryManager_t memoryManager;

uint64_t amount_of_blocks()
{
  int amount = memoryManager.memoryAvailable / BLOCK_SIZE;
  if(amount < MAX_BLOCKS)
    return amount;
  return MAX_BLOCKS;
}

void init_bitmapStatus()
{
  int bitmap_size = amount_of_blocks();
  for (int i = 0; i < bitmap_size; i++)
  {
    memoryManager.bitmap[i] = EMPTY;
    memoryManager.group_sizes[i] = 0;
  }
}

void init_memoryManager(void * base_address, uint64_t bytes)
{
  memoryManager.memoryAvailable = bytes;
  uint64_t bytes_taken_by_arrays = (sizeof(uint64_t) + sizeof(uint8_t)) * amount_of_blocks(); //Necesitamos esta cantidad de bytes para bitmap y group_sizes
  memoryManager.memoryAvailable = bytes - bytes_taken_by_arrays;                              // Desde ahora amount_of_blocks devolverá la cantidad de páginas disponibles para nuestro uso
  memoryManager.bitmap = base_address;
  memoryManager.group_sizes = base_address + sizeof(uint8_t)*amount_of_blocks();              // Dejo un espacio de amount_of_blocks
  memoryManager.memoryBaseAddress = (void *)((char *)base_address + bytes_taken_by_arrays);   // La memoria a administrar va a arrancar a partir del final de group_sizes
  init_bitmapStatus();
}

int first_suitable_index(int size)
{
  int i=0;
  int j=0;
  int bitmap_size = amount_of_blocks();
  for (i = 0; i < bitmap_size; i++)
  {
    if(memoryManager.bitmap[i] == EMPTY)
    {
      j++;
      if(j >= size)
        return i-(size-1);
    }
    else
      j = 0;
  }
  return -1;
}

void print_bitmap()
{
  newLine();
  int bitmap_size = amount_of_blocks();
  int j=0;
  for (int i = 0; i < bitmap_size; i++)
  {
    if(memoryManager.bitmap[i] == EMPTY)
      print("0");
    else if(memoryManager.bitmap[i] == TAKEN)
      print("1");
    else
      print("?");
    j++;
    if(j==50)
    {
      print("   --- Blocks ");
      printDec(i-49);
      print("-");
      printDec(i);
      newLine();
      j=0;
    }
  }
  newLine();
  newLine();
  j=0;
  for (int i = 0; i < bitmap_size; i++)
  {
    printDec(memoryManager.group_sizes[i]);
    print("-");
    j++;
    if(j==50)
    {
      newLine();
      j=0;
    }
  }
  newLine();
}

int amount_of_blocks_needed(uint64_t bytes)
{
  int blocks = bytes/BLOCK_SIZE;
  if(bytes % BLOCK_SIZE != 0)
    blocks++;
  return blocks;
}

void* malloc(uint64_t bytes_to_reserve)
{
  // if it's not requesting memory
  if (bytes_to_reserve <= 0)
  {
    return NULL;
  }
  // if it won't ever fit
  int blocks_to_reserve = amount_of_blocks_needed(bytes_to_reserve);
  if (blocks_to_reserve > amount_of_blocks())
  {
    return NULL;
  }
  int index = first_suitable_index(blocks_to_reserve);
  if(index == -1)
  {
    return NULL;
  }
  int i;
  for (i = 0; i < blocks_to_reserve; i++)
    memoryManager.bitmap[index + i] = TAKEN;
  memoryManager.group_sizes[index] = blocks_to_reserve;
  //print_bitmap();
//  *address = (void *)((uint64_t)memoryManager.memoryBaseAddress + index*BLOCK_SIZE);
  return (void *)((uint64_t)memoryManager.memoryBaseAddress + index*BLOCK_SIZE);
}

void* malloc_handler(uint64_t bytes_to_reserve, void** address)
{
  *address = malloc(bytes_to_reserve);
  return *address;
}

void free_handler(void* p)
{
  uint64_t offset = (char*)p - (char*)memoryManager.memoryBaseAddress;
  if(offset % BLOCK_SIZE != 0)
  {
    print("That address is not the beginning of a process!");
    newLine();
    return; // No hago nada porque esa direccion no es el inicio de un bloque
  }
  int index = offset / BLOCK_SIZE;
  if(index < 0 || index >= amount_of_blocks())
  {
    print("That address is outside the memory range!");
    newLine();
    return; // No hago nada porque esa direccion esta por fuera del bitmap
  }
  int group_size = memoryManager.group_sizes[index];
  if(group_size == 0)
  {
    print("That address is either free or belongs to a process that starts at a previous position!");
    newLine();
    return; // No hago nada porque esa direccion no es el inicio de un proceso
  }

  // free bitmap and group_size
  for (int i = 0; i < group_size; i++)
    memoryManager.bitmap[index+i] = EMPTY;
  memoryManager.group_sizes[index] = 0;
  //print_bitmap();
}
